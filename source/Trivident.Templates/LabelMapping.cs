﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Tridion.ContentManager;
using Tridion.ContentManager.ContentManagement;
using Tridion.ContentManager.ContentManagement.Fields;
using Tridion.ContentManager.Templating;
using Tridion.ContentManager.Templating.Assembly;

namespace Trivident.Templates
{
    [TcmTemplateTitle("Label mappings - Label schema to Key-value pairs")]
    [TcmTemplateParameterSchema("resource:Trivident.Templates.Resources.Label Schema Definition Parameters.xsd")]
    public class LabelMapping : LabelMappingAbstract
    {
        private readonly TemplatingLogger LOG = TemplatingLogger.GetLogger(typeof(LabelMapping));

        public override string GetPrefix()
        {
            return this.Prefix ?? "label_";
        }

        public override IDictionary<string, string> GetMappings(Repository repository)
        {
            var labels = new Dictionary<string, string>();
            LOG.Debug("configured webdavUrl " + this.SchemaWebDavUrl);
            var schema = repository.Session.GetObject(this.SchemaWebDavUrl) as Schema;

            Log.Debug("Schema from cm " + schema.Id);
            var schemaUri = new TcmUri(schema.Id);

            var page = GetTcmPage();
            var contextPub = page.ContextRepository;
            Log.Debug($"using context publication {contextPub.Title}");
            var usingItemsFilter = new UsingItemsFilter(repository.Session)
            {
                ItemTypes = new[] { ItemType.Component },
                IncludedVersions = VersionCondition.OnlyLatestVersions
            };
            var usingComponents = schema.GetListUsingItems(usingItemsFilter);

            IList<Component> components = new List<Component>();
            foreach (XmlElement item in usingComponents.SelectNodes("/*/*"))
            {
                var uri = item.GetAttribute("ID");
                uri = uri.ConvertToPublication(contextPub.Id);
                components.Add(Engine.GetObject<Component>(uri));
            }

            Log.Debug($"Found {components.Count()} components based on schema");

            components.ForEach(component => GetLabelsFromComponent(component, labels));

            return labels;
        }
        private void GetLabelsFromComponent(Component component, Dictionary<string, string> labels)
        {
            Log.Info(string.Format("Found component {0}", component.Id));
            var fields = component.Fields();
            var value = fields.Text("value");

            if (!string.IsNullOrEmpty(value))
            {
                Log.Debug("Found component with single value property, using component title as key");
                AddLabelIfNotPresent(component.Title, value, labels);
                return;
            }

            fields.Embeddeds("label").ForEach(item => AddLabelIfNotPresent(item.Text("key"), item.Text("label"), labels));
        }
        private void AddLabelIfNotPresent(string key, string value, Dictionary<string, string> labels)
        {
            if (!labels.ContainsKey(key))
            {
                Log.Debug(string.Format("found label. {0}:{1}", key, value));
                labels.Add(key, value);
            }
        }
    }
}